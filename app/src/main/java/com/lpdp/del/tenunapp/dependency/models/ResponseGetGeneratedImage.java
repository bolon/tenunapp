package com.lpdp.del.tenunapp.dependency.models;

/**
 * Created by User on 18/10/2016.
 */

public class ResponseGetGeneratedImage {
    private boolean error;
    private String message;
    private String exec_result;
    private String filename;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getExec_result() {
        return exec_result;
    }

    public void setExec_result(String exec_result) {
        this.exec_result = exec_result;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }
}
