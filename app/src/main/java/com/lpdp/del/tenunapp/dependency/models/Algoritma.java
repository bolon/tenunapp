package com.lpdp.del.tenunapp.dependency.models;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by User on 24/10/2016.
 */

public class Algoritma extends RealmObject {
    @PrimaryKey
    private String id;
    @SerializedName("nama_algoritma")
    private String namaAlgoritma;
    private String description;
    private RealmList<AlgoritmaParameter> parameters;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNamaAlgoritma() {
        return namaAlgoritma;
    }

    public void setNamaAlgoritma(String namaAlgoritma) {
        this.namaAlgoritma = namaAlgoritma;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public RealmList<AlgoritmaParameter> getParameters() {
        return parameters;
    }

    public void setParameters(RealmList<AlgoritmaParameter> parameters) {
        this.parameters = parameters;
    }
}
