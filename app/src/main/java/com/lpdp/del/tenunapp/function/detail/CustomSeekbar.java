package com.lpdp.del.tenunapp.function.detail;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.v7.widget.AppCompatSeekBar;
import android.util.AttributeSet;


/**
 * Using this file must on behalf of Institut Teknologi Del & Piksel
 */

public class CustomSeekbar extends AppCompatSeekBar {
    private int progress;
    private float minVal;
    private float maxVal;

    public CustomSeekbar(Context context) {
        super(context);
    }

    public CustomSeekbar(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomSeekbar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public CustomSeekbar(Context context, float minVal, float maxVal) {
        super(context);
        this.minVal = minVal;
        this.maxVal = maxVal;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        float thumbX = (this.getThumb().getBounds().exactCenterX());
        float midY = (this.getHeight() / 2) - 15;

        Paint paint = new Paint();
        paint.setStrokeWidth(2);
        paint.setColor(Color.BLACK);
        paint.setTextSize(24);
        paint.setTextAlign(Paint.Align.CENTER);
        paint.setStyle(Paint.Style.FILL);

        progress = (int) (super.getProgress() + minVal);

        setMax((int) maxVal);

        thumbX -= -10;
        canvas.drawText(String.valueOf(progress), thumbX, midY, paint);
    }

    @Override
    public synchronized int getProgress() {
        return (int) (super.getProgress() + minVal);
    }
}
