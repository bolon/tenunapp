package com.lpdp.del.tenunapp.dependency.network;

import com.lpdp.del.tenunapp.dependency.models.RequestBodyGenerateImg;
import com.lpdp.del.tenunapp.dependency.models.ResponseGetAlgoritma;
import com.lpdp.del.tenunapp.dependency.models.ResponseGetData;
import com.lpdp.del.tenunapp.dependency.models.ResponseGetDataMotif;
import com.lpdp.del.tenunapp.dependency.models.ResponseGetGeneratedImage;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Using this file must on behalf of Institut Teknologi Del & Piksel
 */
public interface TenunNetworkInterface {
    @GET("tenun")
    Call<ResponseGetData> getListTenun(@Header("Authorization") String token, @Query("cursor") int cursor, @Query("size") int size);

    @GET("tenun")
    Call<ResponseGetData> getListTenun(@Header("Authorization") String token, @Query("cursor") int cursor, @Query("size") String size);

    @GET("motifTenun")
    Call<ResponseGetDataMotif> getListMotif(@Header("Authorization") String token, @Query("cursor") int cursor, @Query("size") String size);

    @POST("generateImg")
    Call<ResponseGetGeneratedImage> generateImg(@Header("Authorization") String key, @Body RequestBodyGenerateImg body);

    @GET("algoritmaWithParameter")
    Call<ResponseGetAlgoritma> getAlgoritma(@Header("Authorization") String token);
}
