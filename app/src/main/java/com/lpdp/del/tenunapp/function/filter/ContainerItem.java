package com.lpdp.del.tenunapp.function.filter;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lpdp.del.tenunapp.R;

import timber.log.Timber;

/**
 * Using this file must on behalf of Institut Teknologi Del & Piksel
 */

public class ContainerItem extends LinearLayout implements View.OnClickListener {
    private final int CHECKBOX_POSITION = 1;
    private final int TEXTVIEW_POSITION = 0;
    private TextView textView;
    private CheckBox checkBox;

    public ContainerItem(Context context) {
        super(context);
        init();
    }

    public ContainerItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        setBehavior();
    }

    public ContainerItem(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setBehavior();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ContainerItem(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        setBehavior();
    }

    @Override
    public void onClick(View v) {
        try {
            CheckBox checkBox = (CheckBox) ContainerItem.this.getChildAt(CHECKBOX_POSITION);
            if (checkBox.isChecked()) {
                checkBox.setChecked(false);
            } else {
                checkBox.setChecked(true);
            }
        } catch (Exception ex) {
            Timber.i("Please check child at index " + CHECKBOX_POSITION + ex.getMessage());
        }
    }

    private void setBehavior() {
        this.setOnClickListener(this);
    }

    private void init() {
        View v = inflate(getContext(), R.layout.custom_container, this);

        textView = (TextView) v.findViewById(R.id.textTitle);
        checkBox = (CheckBox) v.findViewById(R.id.checkbox_custom_container);

        setBehavior();
    }

    public void setText(String text) {
        try {
            this.textView.setText(text);
        } catch (ClassCastException ex) {
            Timber.i(ex.getMessage());
        }
    }

    public void getText() {
        try {
            TextView checkBox = (TextView) ContainerItem.this.getChildAt(CHECKBOX_POSITION);

            Timber.i(checkBox.getText().toString());
        } catch (Exception ex) {
            Timber.i(ex.getMessage());
        }
    }

    public boolean getCheckboxValue(){
        return this.checkBox.isChecked();
    }
}

