package com.lpdp.del.tenunapp.dependency.models;

import java.util.List;

/**
 * Created by User on 24/10/2016.
 */

public class ResponseGetAlgoritma {
    private boolean success;
    private String message;
    private List<Algoritma> data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Algoritma> getData() {
        return data;
    }

    public void setData(List<Algoritma> data) {
        this.data = data;
    }
}
