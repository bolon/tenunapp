package com.lpdp.del.tenunapp.dependency.modules;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.lpdp.del.tenunapp.dependency.network.TenunNetworkInterface;

import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import dagger.Module;
import dagger.Provides;
import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import timber.log.Timber;

/**
 * Using this file must on behalf of Institut Teknologi Del & Piksel
 */
@Module(
        complete = false,
        library = true
)

public class APIModule {
    public static final String ENDPOINT = "https://www.jtenun.com/tenunapi/";
    public static final String ACCESS_TOKEN_TEMP = "Jt3nuN_20161130";

    @Provides
    Retrofit provideRetrofit(Call.Factory callFactory, Gson gson) {
        return new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .callFactory(callFactory)
                .baseUrl(ENDPOINT)
                .build();
    }

    @Singleton
    @Provides
    Gson provideGson() {
        return new GsonBuilder()
                .create();
    }

    @Singleton
    @Provides
    Call.Factory provideCallFactory() {
        // creating a KeyStore containing our trusted CAs
        SSLContext sslContext;

        try {
            TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }

                @Override
                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }

                @Override
                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                }
            }};

            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new SecureRandom());

            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

            return new OkHttpClient().newBuilder()
                    .addInterceptor(loggingInterceptor)   //if something wrong
                    .connectTimeout(1, TimeUnit.MINUTES)
                    .readTimeout(3, TimeUnit.MINUTES)
                    .writeTimeout(3, TimeUnit.MINUTES)
                    .hostnameVerifier((hostname, session) -> true)
                    .sslSocketFactory(sc.getSocketFactory(), new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public X509Certificate[] getAcceptedIssuers() {
                            return new X509Certificate[]{};
                        }
                    })
                    .build();
        } catch (Exception e) {
            Timber.i("Exception : " + e);
            return null;
        }
    }

    @Singleton
    @Provides
    TenunNetworkInterface provideService(Retrofit retrofit) {
        return retrofit.create(TenunNetworkInterface.class);
    }
}
