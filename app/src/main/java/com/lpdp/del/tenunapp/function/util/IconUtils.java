package com.lpdp.del.tenunapp.function.util;

import android.content.Context;

import com.joanzapata.iconify.Icon;
import com.joanzapata.iconify.IconDrawable;
import com.lpdp.del.tenunapp.R;

/**
 * Using this file must on behalf of Institut Teknologi Del & Piksel
 */

public class IconUtils {
    public static IconDrawable getIconDrawable(Context context, Icon iconName) {
        return new IconDrawable(context, iconName)
                .colorRes(R.color.colorIcon)
                .actionBarSize();
    }

    public static IconDrawable getIconDrawable(Context context, Icon iconName, int sizeDp) {
        return new IconDrawable(context, iconName)
                .colorRes(R.color.colorIcon)
                .sizeDp(sizeDp);
    }

    public static IconDrawable getIconDrawableWithColor(Context context, Icon iconName, int colorRes) {
        return new IconDrawable(context, iconName)
                .color(colorRes)
                .actionBarSize();
    }
}
