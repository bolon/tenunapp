package com.lpdp.del.tenunapp.dependency.modules;

import android.app.Application;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.realm.Realm;

/**
 * Using this file must on behalf of Institut Teknologi Del & Piksel
 */

@Module(
        complete = false,
        library = true
)
public class DataModule {
    @Provides
    Realm provideRealm(Application application) {
        return Realm.getDefaultInstance();
    }
}
