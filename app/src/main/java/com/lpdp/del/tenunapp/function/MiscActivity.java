package com.lpdp.del.tenunapp.function;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.lpdp.del.tenunapp.R;

import java.io.IOException;
import java.io.InputStream;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

public class MiscActivity extends AppCompatActivity {
    public static final String KEY_ID_MOTIF = "id_motif_key";

    @BindView(R.id.iv_img_quilting)
    ImageView iv_quilting;
    @BindView(R.id.iv_img_warping)
    ImageView iv_warping;
    @BindView(R.id.iv_img_nps)
    ImageView iv_nps;

    public static Intent createIntent(Context context) {
        return new Intent(context, MiscActivity.class);
    }

    public static Intent createIntent(Context context, String id_motif) {
        Intent intent = new Intent(context, MiscActivity.class);
        intent.putExtra(KEY_ID_MOTIF, id_motif);

        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_misc);
        ButterKnife.bind(this);

        if (getIntent() != null) {
            String nama_motif = getIntent().getStringExtra(KEY_ID_MOTIF);
            for (int i = 1; i <= 3; i++) {
                try {
                    InputStream ims = getAssets().open("img/" + nama_motif + "/" + i + ".jpg");
                    Drawable d = Drawable.createFromStream(ims, null);
                    if (i == 1) {
                        iv_quilting.setImageDrawable(d);
                    } else if (i == 2) {
                        iv_warping.setImageDrawable(d);
                    } else if (i == 3) {
                        iv_nps.setImageDrawable(d);
                    }

                    ims.close();
                } catch (IOException ex) {
                    return;
                }
            }
            try {
                InputStream ims = getAssets().open("img/readme.txt");

                Timber.i(ims.toString());
            } catch (IOException ex) {
                return;
            }
        } else {
            iv_quilting.setImageResource(R.mipmap.gen_imgq);
            iv_warping.setImageResource(R.mipmap.gen_imgw);
            iv_nps.setImageResource(R.mipmap.gen_imgnps);
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
}
