package com.lpdp.del.tenunapp.function.splash;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.lpdp.del.tenunapp.R;
import com.lpdp.del.tenunapp.R2;
import com.lpdp.del.tenunapp.dependency.App;
import com.lpdp.del.tenunapp.dependency.models.ResponseGetAlgoritma;
import com.lpdp.del.tenunapp.dependency.models.ResponseGetData;
import com.lpdp.del.tenunapp.dependency.models.ResponseGetDataMotif;
import com.lpdp.del.tenunapp.dependency.modules.APIModule;
import com.lpdp.del.tenunapp.dependency.network.TenunNetworkInterface;
import com.lpdp.del.tenunapp.function.main.MainActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class SplashScreenActivity extends AppCompatActivity {
    private static String ACTION_TITLE_CONTINUE = "CONTINUE";
    private static String ACTION_TITLE_REFRESH = "REFRESH";
    private boolean r1, r2, r3;

    @Inject
    Realm realm;
    @Inject
    TenunNetworkInterface tenunNetworkInterface;
    @BindView(R2.id.spinnerIcon)
    TextView textView;

    @BindView(R2.id.activity_splash_screen)
    RelativeLayout relativeLayout;

    private String DEFAULT_SIZE = "all";
    private int DEFAULT_CURSOR = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        App.get(getApplicationContext()).getInjector().inject(this);
        ButterKnife.bind(this);

        getSupportActionBar().hide();

        fetchData();
    }

    private void fetchData() {
        tenunNetworkInterface.getListMotif(APIModule.ACCESS_TOKEN_TEMP, DEFAULT_CURSOR, DEFAULT_SIZE).enqueue(new Callback<ResponseGetDataMotif>() {
            @Override
            public void onResponse(Call<ResponseGetDataMotif> call, Response<ResponseGetDataMotif> response) {
                if (response.isSuccessful()) {
                    realm.beginTransaction();
                    realm.executeTransactionAsync(realmInstance -> realmInstance.insertOrUpdate(response.body().getData()));
                    realm.commitTransaction();
                    r2 = true;
                } else {
                    Timber.i(String.valueOf(response.code()));
                }
            }

            @Override
            public void onFailure(Call<ResponseGetDataMotif> call, Throwable t) {
                createSnackbar(getResources().getString(R.string.ack_fail_to_connect) + t.getLocalizedMessage(), true).show();
                Timber.i("ASD " + t.getMessage());
            }
        });

        tenunNetworkInterface.getAlgoritma(APIModule.ACCESS_TOKEN_TEMP).enqueue(new Callback<ResponseGetAlgoritma>() {
            @Override
            public void onResponse(Call<ResponseGetAlgoritma> call, Response<ResponseGetAlgoritma> response) {
                if (response.isSuccessful()) {

                    realm.beginTransaction();
                    realm.executeTransactionAsync(realm12 -> realm12.insertOrUpdate(response.body().getData()));
                    realm.commitTransaction();

                    r3 = true;
                }
            }

            @Override
            public void onFailure(Call<ResponseGetAlgoritma> call, Throwable t) {
                createSnackbar(getResources().getString(R.string.ack_fail_to_connect) + t.getLocalizedMessage(), true).show();
                Timber.i("ASD " + t.getMessage());
            }
        });

        tenunNetworkInterface.getListTenun(APIModule.ACCESS_TOKEN_TEMP, DEFAULT_CURSOR, DEFAULT_SIZE).enqueue(new Callback<ResponseGetData>() {
            @Override
            public void onResponse(Call<ResponseGetData> call, Response<ResponseGetData> response) {
                if (response.isSuccessful()) {
                    realm.beginTransaction();
                    realm.executeTransactionAsync(realmInstance -> realmInstance.insertOrUpdate(response.body().getData()));
                    realm.commitTransaction();
                    r1 = true;

                    startActivity(MainActivity.createIntent(getApplicationContext()));
                    SplashScreenActivity.this.finish();
                }
            }

            @Override
            public void onFailure(Call<ResponseGetData> call, Throwable t) {
                createSnackbar(getResources().getString(R.string.ack_fail_to_connect) + t.getLocalizedMessage(), true).show();
                Timber.i("ASD " + t.getMessage());
            }
        });
    }

    private boolean checkStatusRequest() {
        return (r1 & r2 & r3);
    }

    private Snackbar createSnackbar(String message, boolean isAllowedToContinue) {
        if (isAllowedToContinue) {
            return Snackbar.make(relativeLayout, message, Snackbar.LENGTH_INDEFINITE)
                    .setAction(ACTION_TITLE_CONTINUE, v -> {
                        startActivity(MainActivity.createIntent(getApplicationContext()));
                        SplashScreenActivity.this.finish();
                    });
        } else {
            return Snackbar.make(relativeLayout, message, Snackbar.LENGTH_INDEFINITE)
                    .setAction(ACTION_TITLE_REFRESH, v -> SplashScreenActivity.this.recreate());
        }
    }
}
