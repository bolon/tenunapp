package com.lpdp.del.tenunapp.function.main;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.widget.TextView;
import android.widget.Toast;

import com.joanzapata.iconify.Icon;
import com.joanzapata.iconify.IconDrawable;
import com.joanzapata.iconify.fonts.TypiconsIcons;
import com.lpdp.del.tenunapp.R;
import com.lpdp.del.tenunapp.R2;
import com.lpdp.del.tenunapp.function.browse.BrowseMotifActivity;
import com.lpdp.del.tenunapp.function.browse.BrowseMotifFragment;
import com.lpdp.del.tenunapp.function.browse.BrowseTenunActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements HomeFragment.OnFragmentHomeListener, BrowseMotifFragment.OnFragmentInteractionListener {
    @BindView(R2.id.tabLayout)
    TabLayout tabLayout;

    @BindView(R2.id.viewPagerMain)
    ViewPager viewPager;

    private boolean doubleBackToExitPressedOnce = false;

    public static Intent createIntent(Context context) {
        return new Intent(context, MainActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        setViewpager();
        setTab();
    }

    private void setViewpager() {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        adapter.addFrag(HomeFragment.newInstance(), "Home");
        adapter.addFrag(BrowseMotifFragment.newInstance(), "Generate New Pattern");
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(2);

        tabLayout.setupWithViewPager(viewPager);
    }

    private void setTab() {
        TextView tabBeranda = (TextView) LayoutInflater.from(this).inflate(R.layout.tab_layout_item, null);
        tabBeranda.setText(R.string.tab_home_text);
        tabBeranda.setCompoundDrawablesWithIntrinsicBounds(null, geticonDrawable(TypiconsIcons.typcn_home_outline), null, null);

        tabLayout.getTabAt(0).setCustomView(tabBeranda);

        TextView tabGenerate = (TextView) LayoutInflater.from(this).inflate(R.layout.tab_layout_item, null);
        tabGenerate.setText(R.string.tab_home_generate);
        tabGenerate.setCompoundDrawablesWithIntrinsicBounds(null, geticonDrawable(TypiconsIcons.typcn_image_outline), null, null);

        tabLayout.getTabAt(1).setCustomView(tabGenerate);
    }

    IconDrawable geticonDrawable(Icon iconName) {
        return new IconDrawable(this, iconName)
                .colorRes(R.color.colorIcon)
                .actionBarSize();
    }

    @Override
    public void onButtonGenerateClicked() {
        startActivity(BrowseMotifActivity.createIntent(getApplicationContext()));
    }

    @Override
    public void onButtonBrowseClicked() {
        startActivity(BrowseTenunActivity.createIntent(getApplicationContext()));
    }

    @Override
    public void onFragmentInteraction(Uri uri) {
        //STUB
    }

    private void setToolbar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Press BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(() -> doubleBackToExitPressedOnce = false, 1500);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> fragmentList = new ArrayList<>();
        private final List<String> fragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            fragmentList.add(fragment);
            fragmentTitleList.add(title);
        }
    }
}
