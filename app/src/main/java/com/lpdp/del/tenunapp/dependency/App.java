package com.lpdp.del.tenunapp.dependency;

import android.app.Application;
import android.content.Context;

import com.joanzapata.iconify.Iconify;
import com.joanzapata.iconify.fonts.FontAwesomeModule;
import com.joanzapata.iconify.fonts.IoniconsModule;
import com.joanzapata.iconify.fonts.TypiconsModule;
import com.lpdp.del.tenunapp.dependency.modules.AppModule;

import dagger.ObjectGraph;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import timber.log.Timber;

/**
 * Using this file must on behalf of Institut Teknologi Del & Piksel
 */
public class App extends Application {
    private final static int SCHEMA_VERSION = 1;
    private ObjectGraph injector;

    @Override
    public void onCreate() {
        super.onCreate();

        //Timber for logging -> no pun intended
        Timber.plant(new Timber.DebugTree());

        // Configure default configuration for Realm
        RealmConfiguration realmConfig = new RealmConfiguration.Builder(this)
                .deleteRealmIfMigrationNeeded()
                .schemaVersion(SCHEMA_VERSION)
                .build();
        Realm.setDefaultConfiguration(realmConfig);

        //Iconify
        Iconify
                .with(new FontAwesomeModule())
                .with(new TypiconsModule())
                .with(new IoniconsModule());

        injector = ObjectGraph.create(new AppModule(this));
    }

    public static App get(Context context) {
        return (App) context.getApplicationContext();
    }

    public ObjectGraph getInjector() {
        return injector;
    }
}
