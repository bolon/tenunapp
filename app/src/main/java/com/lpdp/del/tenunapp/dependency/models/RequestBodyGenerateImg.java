package com.lpdp.del.tenunapp.dependency.models;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by User on 18/10/2016.
 */

public class RequestBodyGenerateImg {
    public static String ALGO_IMG_QUILTING = "img_quilting";
    public static String ALGO_IMG_WARPING = "img_warping";
    public static String ALGO_NON_PARAMETRIC_SAMPLING = "non_parametric_sample";

    //TODO : CEK THEN GO FOR IT (PUT ENUM IT)

    private String sourceFile;
    private String algoritma;

    @SerializedName("treshold_similarity")
    @Nullable
    private double tresholdSimilarity;

    @SerializedName("interpolar_method")
    @Nullable
    private String interpolarMode;

    @SerializedName("interpolar_radius")
    @Nullable
    private int radius;

    @SerializedName("interpolar_power")
    @Nullable
    private int power;

    public RequestBodyGenerateImg(String sourceFile) {
        this.sourceFile = sourceFile;
        this.algoritma = ALGO_IMG_QUILTING;
    }

    public RequestBodyGenerateImg(String sourceFile, String algoritma, float tresholdSimilarity) {
        this.sourceFile = sourceFile;
        this.algoritma = algoritma;
        this.tresholdSimilarity = tresholdSimilarity;
    }

    public RequestBodyGenerateImg(String sourceFile, String algoritma, int radius, int power) {
        this.sourceFile = sourceFile;
        this.algoritma = algoritma;
        this.radius = radius;
        this.power = power;
    }

    public String getSourceFile() {
        return sourceFile;
    }

    public void setSourceFile(String sourceFile) {
        this.sourceFile = sourceFile;
    }

    public String getAlgoritma() {
        return algoritma;
    }

    public void setAlgoritma(String algoritma) {
        this.algoritma = algoritma;
    }

    @Nullable
    public double getTresholdSimilarity() {
        return tresholdSimilarity;
    }

    public void setTresholdSimilarity(@Nullable double tresholdSimilarity) {
        this.tresholdSimilarity = tresholdSimilarity;
    }

    @Nullable
    public int getPower() {
        return power;
    }

    public void setPower(@Nullable int power) {
        this.power = power;
    }

    @Nullable
    public int getRadius() {
        return radius;
    }

    public void setRadius(@Nullable int radius) {
        this.radius = radius;
    }

    @Nullable
    public String getInterpolarMode() {
        return interpolarMode;
    }

    public void setInterpolarMode(@Nullable String interpolarMode) {
        this.interpolarMode = interpolarMode;
    }
}
