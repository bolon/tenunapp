package com.lpdp.del.tenunapp.function.browse;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.joanzapata.iconify.widget.IconTextView;
import com.lpdp.del.tenunapp.R;
import com.lpdp.del.tenunapp.R2;
import com.lpdp.del.tenunapp.dependency.App;
import com.lpdp.del.tenunapp.dependency.models.MotifTenun;
import com.lpdp.del.tenunapp.dependency.models.Tenun;
import com.lpdp.del.tenunapp.dependency.network.TenunNetworkInterface;
import com.lpdp.del.tenunapp.function.detail.DetailMotifActivity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;

public class BrowseMotifActivity extends AppCompatActivity implements BrowseMotifRecyclerViewAdapter.OnClickItemMotifListener, BrowseMotifRecyclerViewAdapter.OnIdShownListener {
    public final static int REQUEST_CODE = 4;
    private static Parcelable SAVED_INSTANCE;
    private final String EMPTY_LIST = "emptyList";
    private final String ERROR = "error";
    private final String LOADING = "loading";
    private final String SUCCESS = "success";
    private final int SPAN_COUNT = 3;   //CHANGE THIS FOR SPAN COUNT, WE WILL REVISIT THIS THING AFTER MAIN FUNCTION DONE

    @BindView(R2.id.browseMotifCoordinatorLayout)
    CoordinatorLayout coordinatorLayout;
    @BindView(R2.id.toolbarBrowseMotif)
    Toolbar toolbar;
    @BindView(R2.id.recyclerViewListMotif)
    RecyclerView recyclerView;
    @BindView(R2.id.spinnerIcon)
    IconTextView spinner;
    @Inject
    TenunNetworkInterface tenunNetworkInterface;
    @Inject
    Realm realm;
    List<MotifTenun> listMotif = Collections.EMPTY_LIST;
    List<Object> listObject = new ArrayList<>();
    BrowseMotifRecyclerViewAdapter adapter;
    AutoFitGridLayoutManager gridLayoutManager;
    private ScaleInAnimationAdapter scaleInAnimationAdapter;

    public static Intent createIntent(Context context) {
        return new Intent(context, BrowseMotifActivity.class).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browse_motif);

        App.get(getApplicationContext()).getInjector().inject(this);
        ButterKnife.bind(this);

        setToolbar();

        adapter = new BrowseMotifRecyclerViewAdapter(listObject, this, this);
        scaleInAnimationAdapter = new ScaleInAnimationAdapter(adapter);
        recyclerView.setAdapter(scaleInAnimationAdapter);

        gridLayoutManager = new AutoFitGridLayoutManager(getApplicationContext(), 230);
        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                switch (adapter.getItemViewType(position)) {
                    case BrowseMotifRecyclerViewAdapter.MOTIF:
                        return 1;
                    case BrowseMotifRecyclerViewAdapter.HEADER_MOTIF:
                        return gridLayoutManager.getSpanCount();
                    default:
                        return 0;
                }
            }
        });
        recyclerView.setLayoutManager(gridLayoutManager);

        if (SAVED_INSTANCE == null) {
            populateInitialData();
        }
    }

    private void populateInitialData() {
        realm.executeTransactionAsync(realm1 -> {
            listMotif = realm1.copyFromRealm(realm1.where(MotifTenun.class).findAll().sort("idTenun"));
        }, () -> {
            adapter.setData(populateNewListObject(listMotif));
            updateLayout(SUCCESS);
        });
    }

    private List<Object> populateNewListObject(List<MotifTenun> listMotif) {
        List<Object> tempListObject = new ArrayList<>();

        String idTenun = listMotif.get(0).getIdTenun();
        tempListObject.add(idTenun);

        for (MotifTenun motif : listMotif) {
            if (!idTenun.equals(motif.getIdTenun())) {
                idTenun = motif.getIdTenun();
                tempListObject.add(idTenun);
                tempListObject.add(motif);

            } else {
                tempListObject.add(motif);
            }
        }

        return tempListObject;
    }

    private void updateLayout(String status) {
        switch (status) {
            case SUCCESS:
                spinner.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
                break;
            case EMPTY_LIST:
                spinner.setText("{fa-info 200%}  No data found");
                break;
            case ERROR:
                spinner.setText("{fa-info 200%} Error");
                break;
            case LOADING:
                recyclerView.setVisibility(View.GONE);
                spinner.setVisibility(View.VISIBLE);
                break;
            default:
                break;
        }
    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (recyclerView.getLayoutManager() instanceof GridLayoutManager) {
            GridLayoutManager gridLayoutManager = (GridLayoutManager) recyclerView.getLayoutManager();
            SAVED_INSTANCE = gridLayoutManager.onSaveInstanceState();
        } else {
            LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            SAVED_INSTANCE = linearLayoutManager.onSaveInstanceState();
        }
    }


    @Override
    public void OnClickItemMotif(String idMotif, View imageThumb) {
        ActivityOptionsCompat optionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(this, imageThumb, "thumb");

        startActivity(DetailMotifActivity.createIntent(getApplicationContext(), idMotif), optionsCompat.toBundle());
    }

    @Override
    protected void onDestroy() {
        SAVED_INSTANCE = null;
        super.onDestroy();
    }

    @Override
    public String onIdLoaded(String id) {
        return realm.where(Tenun.class).equalTo("id", id).findFirst().getNamaTenun();
    }
}
