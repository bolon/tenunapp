package com.lpdp.del.tenunapp.function.detail;

import android.Manifest;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.GlideBitmapDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;
import com.joanzapata.iconify.widget.IconTextView;
import com.lpdp.del.tenunapp.R;
import com.lpdp.del.tenunapp.R2;
import com.lpdp.del.tenunapp.dependency.App;
import com.lpdp.del.tenunapp.dependency.models.Algoritma;
import com.lpdp.del.tenunapp.dependency.models.AlgoritmaParameter;
import com.lpdp.del.tenunapp.dependency.models.MotifTenun;
import com.lpdp.del.tenunapp.dependency.models.RequestBodyGenerateImg;
import com.lpdp.del.tenunapp.dependency.models.ResponseGetGeneratedImage;
import com.lpdp.del.tenunapp.dependency.modules.APIModule;
import com.lpdp.del.tenunapp.dependency.network.TenunNetworkInterface;
import com.lpdp.del.tenunapp.function.MiscActivity;
import com.lpdp.del.tenunapp.function.filter.ContainerItem;
import com.lpdp.del.tenunapp.function.imageproc.PreviewActivity;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class DetailMotifActivity extends AppCompatActivity {
    public static final String KEY_ID_MOTIF = "id_key";
    private static final String KEY_BUNDLE_SAVED_URL = "keyGeneratedURL";
    private static final int REQUEST_WRITE_STORAGE = 112;
    public static String INTP_MODE = "interpolar_method";
    public static String INTP_POWER = "interpolar_power";
    public static String INTP_RADIUS = "interpolar_radius";
    @Inject
    Realm realm;
    @Inject
    TenunNetworkInterface tenunNetworkInterface;

    @BindView(R2.id.toolbarDetailMotif)
    Toolbar toolbar;
    @BindView(R2.id.thumbMotif)
    ImageView thumbMotif;
    @BindView(R2.id.textToolbarDetail)
    TextView textToolbar;
    @BindView(R2.id.coordinatorLayoutDetailMotif)
    CoordinatorLayout coordinatorLayout;
    @BindView(R2.id.dividerView)
    View dividerView;
    @BindView(R2.id.containerImgGenerated)
    ImageView containerGenerated;
    @BindView(R2.id.progressBarGenerateResult)
    ProgressBar progressBar;
    @BindView(R2.id.saveBtn)
    IconTextView btnSaveImg;
    @BindView(R2.id.titleResult)
    TextView titleResult;

    List<Object> listParamState = new ArrayList<>();
    MaterialDialog dialog2;
    MotifTenun motif;
    MenuItem generateMenu;
    String generatedUrl = "";
    private Algoritma selectedAlgoritma;
    private HashMap<String, Object> paramValue = new HashMap<>();
    private String generatedImg;
    private String idMotif;
    private String namaMotif;

    public static Intent createIntent(Context context, String id) {
        Intent intent = new Intent(context, DetailMotifActivity.class);
        intent.putExtra(KEY_ID_MOTIF, id);

        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_motif);

        App.get(getApplicationContext()).getInjector().inject(this);
        ButterKnife.bind(this);

        Intent returnIntent = getIntent();
        idMotif = returnIntent.getStringExtra(KEY_ID_MOTIF);
        Timber.i("id motif : " + idMotif);
        motif = realm.where(MotifTenun.class)
                .equalTo("id", idMotif)
                .findFirst();
        String imgUrl = APIModule.ENDPOINT + motif.getImageMotif();

        namaMotif = motif.getNamaMotif();

        Glide.with(getApplicationContext())
                .load(imgUrl)
                .override(300, 300)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .into(thumbMotif);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        thumbMotif.setOnClickListener(v -> startActivity(PreviewActivity.createIntent(getApplicationContext(), imgUrl)));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.detail_motif_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_generate:
                generateMenu = item;
                generateMenu.setEnabled(false);
                createDialog();
                break;
            case R.id.action_view_sample:
                startActivity(MiscActivity.createIntent(getApplicationContext(), namaMotif));
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void createDialog() {
        List<Algoritma> listAlgo = realm.copyFromRealm(realm.where(Algoritma.class).findAll());
        List<String> algoTitle = new ArrayList<>();

        for (Algoritma a : listAlgo) {
            algoTitle.add(a.getNamaAlgoritma());
        }

        View dialogView = LayoutInflater.from(this).inflate(R.layout.layout_spinner_algorithm, coordinatorLayout, false);
        LinearLayout layoutContainerParam = (LinearLayout) dialogView.findViewById(R.id.containerParam);

        dialog2 = new MaterialDialog.Builder(DetailMotifActivity.this)
                .customView(dialogView, true)
                .build();

        dialog2.getWindow().setBackgroundDrawable(null);
        dialog2.getWindow().getAttributes().windowAnimations = R.style.DialogTransition;
        WindowManager.LayoutParams wlp = dialog2.getWindow().getAttributes();
        wlp.gravity = Gravity.TOP;
        wlp.width = WindowManager.LayoutParams.MATCH_PARENT;

        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, algoTitle);
        Spinner spinner = (Spinner) dialog2.findViewById(R.id.listAlgo);
        spinner.setAdapter(spinnerAdapter);

        Button btnGenerate = (Button) dialog2.findViewById(R.id.btnGenerate);

        dialog2.setOnDismissListener(dialog -> generateMenu.setEnabled(true));

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                listParamState.clear();
                layoutContainerParam.removeAllViews();

                selectedAlgoritma = realm.where(Algoritma.class).equalTo("namaAlgoritma", listAlgo.get(position).getNamaAlgoritma()).findFirst();
                List<AlgoritmaParameter> algoritmaParameters = selectedAlgoritma.getParameters();

                for (AlgoritmaParameter alPa : algoritmaParameters) {
                    layoutContainerParam.addView(setTextHint(alPa.getNamaParameter()));

                    if (alPa.getType().equals("String")) {
                        List<String> listInterpolarMode = new ArrayList<>();
                        listInterpolarMode.add("Invdist");
                        listInterpolarMode.add("Nearest");
                        listInterpolarMode.add("None");

                        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(DetailMotifActivity.this, android.R.layout.simple_dropdown_item_1line, listInterpolarMode);
                        Spinner spinner = new Spinner(getApplicationContext());
                        spinner.setPopupBackgroundResource(R.color.md_material_blue_800);
                        spinner.setAdapter(spinnerAdapter);

                        layoutContainerParam.addView(spinner);
                        Timber.i(alPa.getNamaParameter());
                        paramValue.put(alPa.getNamaParameter(), spinner);
                    } else if (alPa.getType().equals("Float")) {
                        CustomSeekbar customSeekbar = getCustomSeekbar(alPa.getMinValue(), alPa.getMaxValue());
                        layoutContainerParam.addView(customSeekbar);
                        listParamState.add(customSeekbar);

                        paramValue.put(alPa.getNamaParameter(), customSeekbar);
                    } else if (alPa.getType().equals("int")) {
                        CustomSeekbar customSeekbar = getCustomSeekbar(alPa.getMinValue(), alPa.getMaxValue());
                        layoutContainerParam.addView(customSeekbar);
                        listParamState.add(customSeekbar);
                        paramValue.put(alPa.getNamaParameter(), customSeekbar);
                    }
                }
                btnGenerate.setOnClickListener(v -> buttonGenerate());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        dialog2.show();
    }

    private void saveImg() {
        GlideBitmapDrawable bitmapDrawable = (GlideBitmapDrawable) containerGenerated.getDrawable();
        Bitmap imgBitmap = bitmapDrawable.getBitmap();

        File filePath = android.os.Environment.getExternalStorageDirectory();
        File dir = new File(filePath.getAbsolutePath() + File.separator + "TenunImg" + File.separator);

        if (!dir.exists())
            dir.mkdirs();

        File file = new File(dir, generatedImg + ".jpg");

        try {
            OutputStream outputStream = new FileOutputStream(file);
            imgBitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
            outputStream.flush();
            outputStream.close();

            addImageToGallery(file.getAbsolutePath(), getApplicationContext());
        } catch (IOException ex) {
            Timber.i(ex.getMessage());
        }
    }

    void addImageToGallery(final String filePath, final Context context) {

        ContentValues values = new ContentValues();

        values.put(MediaStore.Images.Media.DATE_TAKEN, System.currentTimeMillis());
        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
        values.put(MediaStore.MediaColumns.DATA, filePath);

        context.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

        Toast.makeText(getApplicationContext(), getResources().getString(R.string.ack_image_saved), Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString(KEY_BUNDLE_SAVED_URL, generatedUrl);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Glide.with(getApplicationContext())
                .load(savedInstanceState.getString(KEY_BUNDLE_SAVED_URL))
                .centerCrop()
                .fitCenter()
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .override(300, 300)
                .into(containerGenerated);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_WRITE_STORAGE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    saveImg();
                } else {
                    Toast.makeText(DetailMotifActivity.this, "Please allow to write to storage", Toast.LENGTH_LONG).show();
                }
                break;
            }
        }
    }

    private CustomSeekbar getCustomSeekbar(String minVal, String maxVal) {
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        float min = Float.parseFloat(minVal);
        float max = Float.parseFloat(maxVal);

        CustomSeekbar seekBar = new CustomSeekbar(this, min, (max - min));
        seekBar.setPadding(32, 16, 32, 16);
        seekBar.setLayoutParams(layoutParams2);

        return seekBar;
    }

    private ContainerItem getCustomCheckBox(String checkBoxTitle) {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        ContainerItem containerItem = new ContainerItem(this);
        containerItem.setPadding(0, 16, 0, 16);
        containerItem.setLayoutParams(layoutParams);
        containerItem.setText(checkBoxTitle);

        return containerItem;
    }

    private TextView setTextHint(String textHint) {

        TextView textView = new TextView(getApplicationContext());

        LinearLayout.LayoutParams textLayoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        textView.setLayoutParams(textLayoutParams);
        textView.setTypeface(Typeface.create("sans-serif-smallcaps", Typeface.NORMAL));
        textView.setPadding(16, 16, 16, 16);
        textView.setTextColor(Color.GRAY);
        textView.setText(textHint);

        return textView;
    }

    private void buttonGenerate() {
        RequestBodyGenerateImg requestBodyGenerateImg = new RequestBodyGenerateImg(motif.getNamaMotif() + ".jpg");
        requestBodyGenerateImg.setAlgoritma(selectedAlgoritma.getNamaAlgoritma());

        btnSaveImg.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        containerGenerated.setImageDrawable(null);

        if (selectedAlgoritma.getNamaAlgoritma().equals(RequestBodyGenerateImg.ALGO_IMG_QUILTING)) {
            CustomSeekbar seekbar = (CustomSeekbar) paramValue.get("treshold_similarity");
            requestBodyGenerateImg.setTresholdSimilarity((double) seekbar.getProgress());
        } else if (selectedAlgoritma.getNamaAlgoritma().equals(RequestBodyGenerateImg.ALGO_IMG_WARPING)) {
            Spinner spinner = (Spinner) paramValue.get(INTP_MODE);
            CustomSeekbar seekbarIntpPower = (CustomSeekbar) paramValue.get(INTP_POWER);
            CustomSeekbar seekbarIntpRadius = (CustomSeekbar) paramValue.get(INTP_RADIUS);

            requestBodyGenerateImg.setInterpolarMode((String) spinner.getSelectedItem());
            requestBodyGenerateImg.setPower(seekbarIntpPower.getProgress());
            requestBodyGenerateImg.setRadius(seekbarIntpRadius.getProgress());
        } else
            requestBodyGenerateImg = new RequestBodyGenerateImg(motif.getNamaMotif() + ".jpg");

        tenunNetworkInterface.generateImg(APIModule.ACCESS_TOKEN_TEMP, requestBodyGenerateImg).enqueue(new Callback<ResponseGetGeneratedImage>() {
            @Override
            public void onResponse(Call<ResponseGetGeneratedImage> call, Response<ResponseGetGeneratedImage> response) {

                if (response.isSuccessful()) {
                    String newImgUrl = response.body().getExec_result();
                    generatedImg = response.body().getFilename();

                    Target target = new SimpleTarget() {
                        @Override
                        public void onResourceReady(Object resource, GlideAnimation glideAnimation) {
                            containerGenerated.setImageDrawable((Drawable) resource);
                            containerGenerated.setOnClickListener(v -> startActivity(PreviewActivity.createIntent(getApplicationContext(), newImgUrl)));
                            progressBar.setVisibility(View.GONE);
                            btnSaveImg.setVisibility(View.VISIBLE);

                            btnSaveImg.setOnClickListener(v -> {
                                boolean hasPermission = (ContextCompat.checkSelfPermission(getApplicationContext(),
                                        Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);

                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                    if (!hasPermission) {
                                        ActivityCompat.requestPermissions(DetailMotifActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_STORAGE);
                                    } else {
                                        saveImg();
                                    }
                                } else {
                                    saveImg();
                                }
                            });
                        }
                    };
                    Glide.with(getApplicationContext())
                            .load(newImgUrl)
                            .centerCrop()
                            .fitCenter()
                            .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                            .override(300, 300)
                            .into(target);

                    generatedUrl = response.body().getExec_result();
                } else {
                    titleResult.setText("Error (" + response.code() + ")");
                    Snackbar snackbar = Snackbar.make(coordinatorLayout, "Error to generate", Snackbar.LENGTH_INDEFINITE);
                    snackbar.setAction("DISMISS", v -> {
                        snackbar.dismiss();
                        titleResult.setText(R.string.generated_text);
                    });
                    snackbar.show();
                    progressBar.setVisibility(View.GONE);

                    generateMenu.setEnabled(true);
                }
            }

            @Override
            public void onFailure(Call<ResponseGetGeneratedImage> call, Throwable t) {
                titleResult.setText(t.getLocalizedMessage());
                Snackbar snackbar = Snackbar.make(coordinatorLayout, "Error to generate", Snackbar.LENGTH_INDEFINITE);
                snackbar.setAction("DISMISS", v -> {
                    snackbar.dismiss();
                    titleResult.setText(R.string.generated_text);
                });
                snackbar.show();
                progressBar.setVisibility(View.GONE);

                generateMenu.setEnabled(true);
            }
        });

        if (dialog2.isShowing()) {
            dialog2.dismiss();
        }
    }
}
