package com.lpdp.del.tenunapp.function.filter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CheckBox;

import com.joanzapata.iconify.fonts.IoniconsIcons;
import com.lpdp.del.tenunapp.R;
import com.lpdp.del.tenunapp.R2;
import com.lpdp.del.tenunapp.function.util.IconUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import butterknife.BindArray;
import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;

public class FilterActivity extends AppCompatActivity {
    public static final int RESULT_CODE = 5;
    public static final String KEY_REGION = "LIST_REGION";
    public static final String KEY_FUNCTION = "LIST_FUNCTION";

    @BindView(R2.id.toolbarFilter)
    Toolbar toolbar;
    @BindView(R2.id.layoutContainer1)
    ContainerItem containerItem;

    @BindViews({R2.id.checkBoxRegion1, R2.id.checkBoxRegion2, R2.id.checkBoxRegion3, R2.id.checkBoxRegion4, R2.id.checkBoxRegion5, R2.id.checkBoxRegion6})
    List<CheckBox> viewCheckBoxListRegion;
    @BindViews({R2.id.checkBoxFunction1, R2.id.checkBoxFunction2, R2.id.checkBoxFunction3, R2.id.checkBoxFunction4, R2.id.checkBoxFunction5})
    List<CheckBox> viewCheckBoxListFunction;
    @BindArray(R2.array.filter_array)
    String[] arrRegion;
    @BindArray(R2.array.filter_function)
    String[] arrFunction;

    private List<String> listRegionName = new ArrayList<>();
    private List<String> listFunctionName = new ArrayList<>();

    private HashMap<Integer, String> hashPairCheckBox = new HashMap<>();

    public static Intent createIntent(Context context) {
        return new Intent(context, FilterActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);

        ButterKnife.bind(this);

        setToolbar();
        setKeyPairCheckBox();
    }

    private void setArr() {
        Collections.addAll(listRegionName, arrRegion);
        Collections.addAll(listFunctionName, arrFunction);
    }

    private void setKeyPairCheckBox() {
        setArr();

        int i = 0;
        for (String s : listRegionName) {
            hashPairCheckBox.put(viewCheckBoxListRegion.get(i).getId(), s);
            i++;
        }

        i = 0;
        for (String s : listFunctionName) {
            hashPairCheckBox.put(viewCheckBoxListFunction.get(i).getId(), s);
            i++;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        exitWithAnimation();
    }

    private void setToolbar() {
        setSupportActionBar(toolbar);

        toolbar.setNavigationIcon(IconUtils.getIconDrawable(getApplicationContext(), IoniconsIcons.ion_android_close));
        toolbar.setNavigationOnClickListener(v -> exitWithAnimation());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.filter_activity_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_reset:
                for (CheckBox c : viewCheckBoxListRegion) {
                    c.setChecked(false);
                }
                break;
            case R.id.action_next:
                Intent returnIntent = new Intent();
                int idCheckBox;

                for (CheckBox c : viewCheckBoxListRegion) {
                    if (!c.isChecked()) {
                        idCheckBox = c.getId();
                        listRegionName.remove(hashPairCheckBox.get(idCheckBox));
                    }
                }

                for (CheckBox c : viewCheckBoxListFunction) {
                    if (!c.isChecked()) {
                        idCheckBox = c.getId();
                        listFunctionName.remove(hashPairCheckBox.get(idCheckBox));
                    }
                }

                returnIntent.putStringArrayListExtra(KEY_REGION, (ArrayList<String>) listRegionName);
                returnIntent.putStringArrayListExtra(KEY_FUNCTION, (ArrayList<String>) listFunctionName);

                setResult(RESULT_CODE, returnIntent);

                exitWithAnimation();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    private void exitWithAnimation() {
        finish();
        overridePendingTransition(R.transition.transition_do_nothing, R.transition.slide_bot);
    }
}
