package com.lpdp.del.tenunapp.function.detail;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.joanzapata.iconify.fonts.IoniconsIcons;
import com.lpdp.del.tenunapp.R;
import com.lpdp.del.tenunapp.R2;
import com.lpdp.del.tenunapp.dependency.App;
import com.lpdp.del.tenunapp.dependency.models.Tenun;
import com.lpdp.del.tenunapp.dependency.modules.APIModule;
import com.lpdp.del.tenunapp.function.imageproc.PreviewActivity;
import com.lpdp.del.tenunapp.function.util.IconUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import timber.log.Timber;

public class DetailTenunActivity extends AppCompatActivity {
    public static final String KEY_ID_TENUN = "id_key";

    @Inject
    Realm realm;
    @BindView(R2.id.toolbarDetailTenun)
    Toolbar toolbar;
    @BindView(R2.id.thumbTenun)
    ImageView thumbTenun;
    @BindView(R2.id.textToolbarDetail)
    TextView textToolbar;
    @BindView(R2.id.descTenun)
    TextView textDescTenun;
    @BindView(R2.id.historyTenun)
    TextView textHistoryTenun;
    @BindView(R2.id.functionTenun)
    TextView textFunctionTenun;
    @BindView(R2.id.originTenun)
    TextView textOriginTenun;
    @BindView(R2.id.titleTenun)
    TextView textTitleTenun;

    private Tenun tenun;

    public static Intent createIntent(Context context, String id) {
        Intent intent = new Intent(context, DetailTenunActivity.class);
        intent.putExtra(KEY_ID_TENUN, id);

        return intent;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_tenun);

        App.get(getApplicationContext()).getInjector().inject(this);
        ButterKnife.bind(this);

        Intent returnIntent = getIntent();
        String idTenun = returnIntent.getStringExtra(KEY_ID_TENUN);

        tenun = realm.where(Tenun.class)
                .equalTo("id", idTenun)
                .findFirst();
        String imgUrl = APIModule.ENDPOINT + tenun.getImageSrc();

        /* for beauty toolbar */
        SimpleTarget target = new SimpleTarget<Bitmap>() {
            @Override
            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                thumbTenun.setImageBitmap(resource);
                Palette palette = Palette.from(resource).generate();
                int defaultColor;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                    defaultColor = getResources().getColor(R.color.colorPrimaryDark, getTheme());
                } else {
                    defaultColor = getResources().getColor(R.color.colorPrimaryDark);
                }

                int newPrimaryColor = palette.getLightVibrantColor(defaultColor);
                int newAccentColor = palette.getDarkVibrantColor(defaultColor);

                toolbar.setBackgroundColor(newPrimaryColor);
                toolbar.setNavigationIcon(IconUtils.getIconDrawableWithColor(getApplicationContext(), IoniconsIcons.ion_android_arrow_back, newAccentColor));
                textToolbar.setTextColor(newAccentColor);
            }
        };

        Glide.with(getApplicationContext())
                .load(imgUrl)
                .asBitmap()
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .asIs()
                .into(target);

        Timber.i(tenun.getImageSrc());
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setContent();

        thumbTenun.setOnClickListener(v -> startActivity(PreviewActivity.createIntent(getApplicationContext(), imgUrl)));
    }

    private void setContent() {
        textHistoryTenun.setTypeface(Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/garamond_regular_bold.ttf"));
        textHistoryTenun.setTextColor(Color.BLACK);
        textHistoryTenun.setText(tenun.getSejarahTenun());

        textDescTenun.setTypeface(Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/myriad_condensed.ttf"));
        textDescTenun.setTextColor(Color.BLACK);
        textDescTenun.setText(tenun.getDeskripsiTenun());

        textTitleTenun.setTypeface(Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/garamond_regular_bold.ttf"));
        textTitleTenun.setTextColor(Color.BLACK);
        textTitleTenun.setText(tenun.getNamaTenun());
    }
}
