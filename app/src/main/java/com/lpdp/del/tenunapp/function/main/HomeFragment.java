package com.lpdp.del.tenunapp.function.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;

import com.afollestad.materialdialogs.MaterialDialog;
import com.joanzapata.iconify.fonts.FontAwesomeIcons;
import com.joanzapata.iconify.fonts.TypiconsIcons;
import com.joanzapata.iconify.widget.IconTextView;
import com.lpdp.del.tenunapp.R;
import com.lpdp.del.tenunapp.R2;
import com.lpdp.del.tenunapp.dependency.App;
import com.lpdp.del.tenunapp.dependency.models.Tenun;
import com.lpdp.del.tenunapp.dependency.network.TenunNetworkInterface;
import com.lpdp.del.tenunapp.function.MiscActivity;
import com.lpdp.del.tenunapp.function.browse.AutoFitGridLayoutManager;
import com.lpdp.del.tenunapp.function.browse.BrowseTenunRecyclerViewAdapter;
import com.lpdp.del.tenunapp.function.detail.DetailTenunActivity;
import com.lpdp.del.tenunapp.function.filter.FilterActivity;
import com.lpdp.del.tenunapp.function.util.IconUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmQuery;
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;
import timber.log.Timber;

public class HomeFragment extends Fragment implements BrowseTenunRecyclerViewAdapter.OnClickItemTenunListener {
    public final static int REQUEST_CODE = 4;
    private static Parcelable SAVED_INSTANCE;
    private final String MODE_LINEAR = "linear";
    private final String MODE_GRID = "grid";
    private final String LAYOUT_EMPTY = "emptyList";
    private final String LAYOUT_ERROR = "error";
    private final String LAYOUT_LOADING = "loading";
    private final String LAYOUT_SUCCESS = "success";
    private final int DEFAULT_WIDTH = 250;

    @BindView(R2.id.browseTenunCoordinatorLayout)
    CoordinatorLayout coordinatorLayout;
    @BindView(R2.id.toolbarBrowseTenun)
    Toolbar toolbar;
    @BindView(R2.id.recyclerViewListTenun)
    RecyclerView recyclerView;
    @BindView(R2.id.spinnerIcon)
    IconTextView spinner;
    @Inject
    TenunNetworkInterface tenunNetworkInterface;
    @Inject
    Realm realm;
    List<Tenun> listTenun = Collections.EMPTY_LIST;
    List<Object> listObject = new ArrayList<>();
    BrowseTenunRecyclerViewAdapter adapter;
    RecyclerView.LayoutManager linearLayoutManager;
    AutoFitGridLayoutManager gridLayoutManager;

    private ScaleInAnimationAdapter scaleInAnimationAdapter;
    private OnFragmentHomeListener listener;
    private View V;
    private MaterialDialog dialog;

    public HomeFragment() {
        // Required empty public constructor
        setHasOptionsMenu(true);
    }

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        V = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, V);
        App.get(getActivity()).getInjector().inject(this);

        setHasOptionsMenu(true);

        ((AppCompatActivity) getActivity()).getDelegate().setSupportActionBar(toolbar);

        return V;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        adapter = new BrowseTenunRecyclerViewAdapter(listObject, this);
        scaleInAnimationAdapter = new ScaleInAnimationAdapter(adapter);
        recyclerView.setAdapter(scaleInAnimationAdapter);

        linearLayoutManager = new LinearLayoutManager(getActivity());
        gridLayoutManager = new AutoFitGridLayoutManager(getActivity(), DEFAULT_WIDTH);
        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                switch (adapter.getItemViewType(position)) {
                    case BrowseTenunRecyclerViewAdapter.TENUN:
                        return 1;
                    case BrowseTenunRecyclerViewAdapter.HEADER_TENUN:
                        return gridLayoutManager.getSpanCount();
                    default:
                        return 0;
                }
            }
        });
        recyclerView.setLayoutManager(gridLayoutManager);

        if (SAVED_INSTANCE == null) {
            populateInitialData();
        }
    }

    private void populateInitialData() {
        realm.executeTransactionAsync(realm1 -> {
            listTenun = realm1.copyFromRealm(realm1.where(Tenun.class).findAll().sort("asalTenun"));
        }, () -> {
            if (!listTenun.isEmpty()) {
                adapter.setData(populateNewListObject(listTenun));
                updateLayout(LAYOUT_SUCCESS);
            } else {
                updateLayout(LAYOUT_EMPTY);
            }
        });
    }

    private void updateLayout(String status) {
        switch (status) {
            case LAYOUT_SUCCESS:
                spinner.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
                break;
            case LAYOUT_EMPTY:
                spinner.setText("{fa-info 200%}  No data found");
                break;
            case LAYOUT_ERROR:
                spinner.setText("{fa-info 200%} Error");
                break;
            case LAYOUT_LOADING:
                recyclerView.setVisibility(View.GONE);
                spinner.setVisibility(View.VISIBLE);
                break;
            default:
                break;
        }
    }

    private List<Object> populateNewListObject(List<Tenun> listTenun) {
        List<Object> tempListObject = new ArrayList<>();

        String asalTenun = listTenun.get(0).getAsalTenun();
        tempListObject.add(asalTenun);

        for (Tenun tenun : listTenun) {
            if (!asalTenun.equals(tenun.getAsalTenun())) {
                asalTenun = tenun.getAsalTenun();
                tempListObject.add(asalTenun);
                tempListObject.add(tenun);

            } else {
                tempListObject.add(tenun);
            }
        }

        return tempListObject;
    }

    private void setRecyclerView(String layoutMode) {
        switch (layoutMode) {
            case MODE_LINEAR:
                recyclerView.setLayoutManager(linearLayoutManager);
                break;
            case MODE_GRID:
                recyclerView.setLayoutManager(gridLayoutManager);
                break;
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.browse_activity_menu, menu);

        menu.findItem(R.id.action_filter).setIcon(IconUtils.getIconDrawable(getActivity(), TypiconsIcons.typcn_filter));
        menu.findItem(R.id.action_set_list_layout).setIcon(IconUtils.getIconDrawable(getActivity(), TypiconsIcons.typcn_th_list_outline));
        menu.findItem(R.id.action_misc).setIcon(IconUtils.getIconDrawable(getActivity(), FontAwesomeIcons.fa_ellipsis_v));

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_filter:
                startActivityForResult(FilterActivity.createIntent(getActivity()), REQUEST_CODE);
                getActivity().overridePendingTransition(R.transition.slide_up, R.transition.transition_do_nothing);
                break;
            case R.id.action_set_list_layout:
                if (recyclerView.getLayoutManager() instanceof GridLayoutManager) {
                    setRecyclerView(MODE_LINEAR);
                    item.setIcon(IconUtils.getIconDrawable(getActivity(), TypiconsIcons.typcn_th_large_outline));
                } else {
                    setRecyclerView(MODE_GRID);
                    item.setIcon(IconUtils.getIconDrawable(getActivity(), TypiconsIcons.typcn_th_list_outline));
                }
                break;
            case R.id.action_misc:
                createMiscDialog();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (resultCode) {
            case FilterActivity.RESULT_CODE:
                updateLayout(LAYOUT_LOADING);
                List<String> filterRegion = data.getStringArrayListExtra(FilterActivity.KEY_REGION);
                List<String> filterFunction = data.getStringArrayListExtra(FilterActivity.KEY_FUNCTION);

                RealmQuery<Tenun> query = realm.where(Tenun.class);

                if (!filterFunction.isEmpty() | !filterRegion.isEmpty()) {
                    for (String s : filterRegion) {
                        if (filterRegion.size() == 1)
                            query.equalTo("asalTenun", s);
                        else
                            query.or().equalTo("asalTenun", s);
                    }

                    for (String s : filterFunction) {
                        if (filterRegion.size() == 0)
                            query.or().equalTo("kegunaanTenun", s);
                        else
                            query.equalTo("kegunaanTenun", s);
                    }

                    listTenun = realm.copyFromRealm(query.findAll().sort("asalTenun"));

                    if (listTenun.size() != 0) {
                        adapter.setData(populateNewListObject(listTenun));
                        updateLayout(LAYOUT_SUCCESS);
                    } else
                        updateLayout(LAYOUT_EMPTY);
                } else {
                    realm.executeTransactionAsync(realm1 -> {
                        listTenun = realm1.copyFromRealm(realm1.where(Tenun.class).findAll().sort("asalTenun"));
                    }, () -> {
                        adapter.setData(populateNewListObject(listTenun));
                        updateLayout(LAYOUT_SUCCESS);
                    });
                }
                break;
            default:
                Timber.i("Default");
                break;
        }
    }

    private void createMiscDialog() {
        View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.layout_dialog_misc, coordinatorLayout, false);

        dialog = new MaterialDialog.Builder(getActivity())
                .customView(dialogView, true)
                .build();

        dialog.getWindow().setBackgroundDrawable(null);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogTransition;
        WindowManager.LayoutParams wlp = dialog.getWindow().getAttributes();
        wlp.gravity = Gravity.TOP;
        wlp.width = WindowManager.LayoutParams.MATCH_PARENT;

        MaterialDialog dialog2 = new MaterialDialog.Builder(getActivity())
                .customView(R.layout.layout_content_about, true)
                .build();

        Button b2 = (Button) dialogView.findViewById(R.id.btn_about);
        b2.setOnClickListener(v -> {
            dialog.dismiss();
            dialog2.show();
        });

        dialog.show();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof HomeFragment.OnFragmentHomeListener) {
            listener = (HomeFragment.OnFragmentHomeListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public void OnClickItemTenun(String idTenun, View imageThumb) {
        ActivityOptionsCompat optionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(), imageThumb, "thumb");

        startActivity(DetailTenunActivity.createIntent(getActivity(), idTenun), optionsCompat.toBundle());
    }

    public interface OnFragmentHomeListener {
        void onButtonGenerateClicked();

        void onButtonBrowseClicked();
    }
}
