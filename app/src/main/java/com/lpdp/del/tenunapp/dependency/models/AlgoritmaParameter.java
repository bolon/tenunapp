package com.lpdp.del.tenunapp.dependency.models;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Using this file must on behalf of Institut Teknologi Del & Piksel
 */

public class AlgoritmaParameter extends RealmObject {
    @PrimaryKey
    private String id;
    @SerializedName("id_algoritma")
    private String idAlgoritma;
    @SerializedName("nama_parameter")
    private String namaParameter;
    @SerializedName("type_algoritma")
    private String type;
    @SerializedName("min_val")
    private String minValue;
    @SerializedName("max_val")
    private String maxValue;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdAlgoritma() {
        return idAlgoritma;
    }

    public void setIdAlgoritma(String idAlgoritma) {
        this.idAlgoritma = idAlgoritma;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMinValue() {
        return minValue;
    }

    public void setMinValue(String minValue) {
        this.minValue = minValue;
    }

    public String getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(String maxValue) {
        this.maxValue = maxValue;
    }

    public String getNamaParameter() {
        return namaParameter;
    }

    public void setNamaParameter(String namaParameter) {
        this.namaParameter = namaParameter;
    }
}
