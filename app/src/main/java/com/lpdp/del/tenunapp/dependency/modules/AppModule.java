package com.lpdp.del.tenunapp.dependency.modules;

/**
 * Using this file must on behalf of Institut Teknologi Del & Piksel
 */

import android.app.Application;

import com.lpdp.del.tenunapp.dependency.App;
import com.lpdp.del.tenunapp.function.browse.BrowseMotifActivity;
import com.lpdp.del.tenunapp.function.browse.BrowseMotifFragment;
import com.lpdp.del.tenunapp.function.browse.BrowseTenunActivity;
import com.lpdp.del.tenunapp.function.detail.DetailMotifActivity;
import com.lpdp.del.tenunapp.function.detail.DetailTenunActivity;
import com.lpdp.del.tenunapp.function.main.HomeFragment;
import com.lpdp.del.tenunapp.function.splash.SplashScreenActivity;

import dagger.Module;
import dagger.Provides;

@Module(
        includes = {
                APIModule.class,
                DataModule.class
        },
        library = true,
        injects = {
                BrowseTenunActivity.class,
                SplashScreenActivity.class,
                DetailTenunActivity.class,
                BrowseMotifActivity.class,
                DetailMotifActivity.class,
                HomeFragment.class,
                BrowseMotifFragment.class
        }
)
public class AppModule {
    private final App application;

    public AppModule(App application) {
        this.application = application;
    }

    @Provides
    Application provideApplication() {
        return application;
    }
}
