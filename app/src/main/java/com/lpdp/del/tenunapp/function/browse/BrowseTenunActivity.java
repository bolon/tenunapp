package com.lpdp.del.tenunapp.function.browse;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.joanzapata.iconify.fonts.TypiconsIcons;
import com.joanzapata.iconify.widget.IconTextView;
import com.lpdp.del.tenunapp.R;
import com.lpdp.del.tenunapp.R2;
import com.lpdp.del.tenunapp.dependency.App;
import com.lpdp.del.tenunapp.dependency.models.Tenun;
import com.lpdp.del.tenunapp.dependency.network.TenunNetworkInterface;
import com.lpdp.del.tenunapp.function.detail.DetailTenunActivity;
import com.lpdp.del.tenunapp.function.filter.FilterActivity;
import com.lpdp.del.tenunapp.function.util.IconUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmQuery;
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;
import timber.log.Timber;

public class BrowseTenunActivity extends AppCompatActivity implements BrowseTenunRecyclerViewAdapter.OnClickItemTenunListener {
    public final static int REQUEST_CODE = 4;
    private static Parcelable SAVED_INSTANCE;
    private final String MODE_LINEAR = "linear";
    private final String MODE_GRID = "grid";
    private final String LAYOUT_EMPTY = "emptyList";
    private final String LAYOUT_ERROR = "error";
    private final String LAYOUT_LOADING = "loading";
    private final String LAYOUT_SUCCESS = "success";
    private final int DEFAULT_WIDTH = 250;

    @BindView(R2.id.browseTenunCoordinatorLayout)
    CoordinatorLayout coordinatorLayout;
    @BindView(R2.id.toolbarBrowseTenun)
    Toolbar toolbar;
    @BindView(R2.id.recyclerViewListTenun)
    RecyclerView recyclerView;
    @BindView(R2.id.spinnerIcon)
    IconTextView spinner;
    @Inject
    TenunNetworkInterface tenunNetworkInterface;
    @Inject
    Realm realm;
    List<Tenun> listTenun = Collections.EMPTY_LIST;
    List<Object> listObject = new ArrayList<>();
    BrowseTenunRecyclerViewAdapter adapter;
    RecyclerView.LayoutManager linearLayoutManager;
    AutoFitGridLayoutManager gridLayoutManager;
    private ScaleInAnimationAdapter scaleInAnimationAdapter;

    public static Intent createIntent(Context context) {
        return new Intent(context, BrowseTenunActivity.class).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browse_tenun);

        App.get(getApplicationContext()).getInjector().inject(this);
        ButterKnife.bind(this);

        setToolbar();

        //IN FACT, WE DIDNT NEED LOAD MORE LISTENER . YET.!
        /*
        /*
        endlessScrollListener = new EndlessScrollListener(recyclerView.getLayoutManager(), pagination) {
            @Override
            public void onLoadMore(int cursor, int size) {
            }
        };
        recyclerView.addOnScrollListener(endlessScrollListener);
        */

        adapter = new BrowseTenunRecyclerViewAdapter(listObject, this);
        scaleInAnimationAdapter = new ScaleInAnimationAdapter(adapter);
        recyclerView.setAdapter(scaleInAnimationAdapter);

        linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        gridLayoutManager = new AutoFitGridLayoutManager(getApplicationContext(), DEFAULT_WIDTH);
        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                switch (adapter.getItemViewType(position)) {
                    case BrowseTenunRecyclerViewAdapter.TENUN:
                        return 1;
                    case BrowseTenunRecyclerViewAdapter.HEADER_TENUN:
                        return gridLayoutManager.getSpanCount();
                    default:
                        return 0;
                }
            }
        });
        recyclerView.setLayoutManager(gridLayoutManager);

        if (SAVED_INSTANCE == null) {
            populateInitialData();
        }
    }

    private void setRecyclerView(String layoutMode) {
        switch (layoutMode) {
            case MODE_LINEAR:
                recyclerView.setLayoutManager(linearLayoutManager);
                break;
            case MODE_GRID:
                recyclerView.setLayoutManager(gridLayoutManager);
                break;
        }
    }

    private void populateInitialData() {
        realm.executeTransactionAsync(realm1 -> {
            listTenun = realm1.copyFromRealm(realm1.where(Tenun.class).findAll().sort("asalTenun"));
        }, () -> {
            if (!listTenun.isEmpty()) {
                adapter.setData(populateNewListObject(listTenun));
                updateLayout(LAYOUT_SUCCESS);
            } else {
                updateLayout(LAYOUT_EMPTY);
            }
        });
    }

    private List<Object> populateNewListObject(List<Tenun> listTenun) {
        List<Object> tempListObject = new ArrayList<>();

        String asalTenun = listTenun.get(0).getAsalTenun();
        tempListObject.add(asalTenun);

        for (Tenun tenun : listTenun) {
            if (!asalTenun.equals(tenun.getAsalTenun())) {
                asalTenun = tenun.getAsalTenun();
                tempListObject.add(asalTenun);
                tempListObject.add(tenun);

            } else {
                tempListObject.add(tenun);
            }
        }

        return tempListObject;
    }

    private void updateLayout(String status) {
        switch (status) {
            case LAYOUT_SUCCESS:
                spinner.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
                break;
            case LAYOUT_EMPTY:
                spinner.setText("{fa-info 200%}  No data found");
                break;
            case LAYOUT_ERROR:
                spinner.setText("{fa-info 200%} Error");
                break;
            case LAYOUT_LOADING:
                recyclerView.setVisibility(View.GONE);
                spinner.setVisibility(View.VISIBLE);
                break;
            default:
                break;
        }
    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.browse_activity_menu, menu);

        menu.findItem(R.id.action_filter).setIcon(IconUtils.getIconDrawable(getApplicationContext(), TypiconsIcons.typcn_filter));
        menu.findItem(R.id.action_set_list_layout).setIcon(IconUtils.getIconDrawable(getApplicationContext(), TypiconsIcons.typcn_th_list_outline));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_filter:
                startActivityForResult(FilterActivity.createIntent(getApplicationContext()), REQUEST_CODE);
                BrowseTenunActivity.this.overridePendingTransition(R.transition.slide_up, R.transition.transition_do_nothing);
                break;
            case R.id.action_set_list_layout:
                if (recyclerView.getLayoutManager() instanceof GridLayoutManager) {
                    setRecyclerView(MODE_LINEAR);
                    item.setIcon(IconUtils.getIconDrawable(getApplicationContext(), TypiconsIcons.typcn_th_large_outline));
                } else {
                    setRecyclerView(MODE_GRID);
                    item.setIcon(IconUtils.getIconDrawable(getApplicationContext(), TypiconsIcons.typcn_th_list_outline));
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (resultCode) {
            case FilterActivity.RESULT_CODE:
                updateLayout(LAYOUT_LOADING);
                List<String> filterRegion = data.getStringArrayListExtra(FilterActivity.KEY_REGION);
                List<String> filterFunction = data.getStringArrayListExtra(FilterActivity.KEY_FUNCTION);

                RealmQuery<Tenun> query = realm.where(Tenun.class);

                if (!filterFunction.isEmpty() | !filterRegion.isEmpty()) {
                    for (String s : filterRegion) {
                        if (filterRegion.indexOf(s) == 0) {
                            query.equalTo("asalTenun", s);
                        } else
                            query.or().equalTo("asalTenun", s);
                    }

                    for (String s : filterFunction) {
                        if (filterFunction.indexOf(s) == 0 & filterRegion.size() == 0) {
                            query.equalTo("kegunaanTenun", s);
                        } else
                            query.or().equalTo("kegunaanTenun", s);
                    }

                    if (!listTenun.isEmpty()) {
                        listTenun = realm.copyFromRealm(query.findAll().sort("asalTenun"));
                        adapter.setData(populateNewListObject(listTenun));
                        updateLayout(LAYOUT_SUCCESS);
                    } else
                        updateLayout(LAYOUT_EMPTY);
                } else {
                    realm.executeTransactionAsync(realm1 -> {
                        listTenun = realm1.copyFromRealm(realm1.where(Tenun.class).findAll().sort("asalTenun"));
                    }, () -> {
                        adapter.setData(populateNewListObject(listTenun));
                        updateLayout(LAYOUT_SUCCESS);
                    });
                }
                break;
            default:
                Timber.i("Default");
                break;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (recyclerView.getLayoutManager() instanceof AutoFitGridLayoutManager) {
            AutoFitGridLayoutManager gridLayoutManager = (AutoFitGridLayoutManager) recyclerView.getLayoutManager();
            SAVED_INSTANCE = gridLayoutManager.onSaveInstanceState();
        } else {
            LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            SAVED_INSTANCE = linearLayoutManager.onSaveInstanceState();
        }
    }

    @Override
    public void OnClickItemTenun(String idTenun, View imageThumb) {
        ActivityOptionsCompat optionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(this, imageThumb, "thumb");

        startActivity(DetailTenunActivity.createIntent(getApplicationContext(), idTenun), optionsCompat.toBundle());
    }

    @Override
    protected void onDestroy() {
        SAVED_INSTANCE = null;
        super.onDestroy();
    }


}
