package com.lpdp.del.tenunapp.function.imageproc;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;
import com.joanzapata.iconify.widget.IconTextView;
import com.lpdp.del.tenunapp.R;
import com.lpdp.del.tenunapp.R2;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

public class PreviewActivity extends AppCompatActivity {
    public static final String URL_KEY_PREVIEW = "key_url_img";
    public static final String RESOURCE_KEY = "key_res_id";

    @BindView(R2.id.previewImg)
    SubsamplingScaleImageView imgContainer;
    @BindView(R2.id.actionRotate)
    IconTextView actionRotate;

    String imgUrl;
    int res_id;

    public static Intent createIntent(Context context, String url) {
        Intent intent = new Intent(context, PreviewActivity.class);
        intent.putExtra(URL_KEY_PREVIEW, url);

        return intent;
    }

    public static Intent createIntent(Context context, int resource_id) {
        Intent intent = new Intent(context, PreviewActivity.class);
        intent.putExtra(RESOURCE_KEY, resource_id);
        Timber.i("intent creation" + resource_id);

        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview);

        ButterKnife.bind(this);

        SimpleTarget target = new SimpleTarget<Bitmap>() {
            @Override
            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                imgContainer.setImage(ImageSource.bitmap(resource));
            }
        };

        if (getIntent() != null) {
            if (getIntent().getStringExtra(URL_KEY_PREVIEW) != null) {
                imgUrl = getIntent().getStringExtra(URL_KEY_PREVIEW);
                Timber.i(imgUrl);

                Glide.with(getApplicationContext())
                        .load(imgUrl)
                        .asBitmap()
                        .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .asIs()
                        .into(target);

            } else if (getIntent().getIntExtra(RESOURCE_KEY, 0) != 0) {
                res_id = getIntent().getIntExtra(RESOURCE_KEY, R.mipmap.jogjakarta_b);
                Glide.with(getApplicationContext())
                        .load(res_id)
                        .asBitmap()
                        .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .asIs()
                        .into(target);
            }
        }

        actionRotate.setOnClickListener(v -> {
            Timber.i(String.valueOf(imgContainer.getOrientation()));

            imgContainer.setOrientation((imgContainer.getOrientation() + 90) % 360);
        });
    }
}
